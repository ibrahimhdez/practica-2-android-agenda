package com.example.ibrahimhernandezjorge.agenda

import android.Manifest
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.Toast
import com.example.ibrahimhernandezjorge.agenda.`interface`.AddContactActivity
import com.example.ibrahimhernandezjorge.agenda.adapters.AdaptadorContacto
import com.example.ibrahimhernandezjorge.agenda.database.DBComunicator
import com.example.ibrahimhernandezjorge.agenda.database.DBContract
import android.app.SearchManager
import android.content.Context
import android.widget.SearchView
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.net.Uri
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import java.util.Locale.filter

class MainActivity : AppCompatActivity(), SearchView.OnQueryTextListener {
    private var exportImportController: ExportImportController? = null
    private var fab: FloatingActionButton? = null
    private var recyclerContactos: RecyclerView? = null
    private var contactos: ArrayList<Contacto>? = null
    private var adapter: AdaptadorContacto? = null
    private var posicionContactoEditado: Int? = null
    private var idContactoEditado: Int? = null
    private var menu: Menu? = null

    companion object {
        const val PERMISSIONS_REQUEST_CALL_PHONE = 0
        const val PERMISSIONS_REQUEST_EXTERNAL_STORAGE = 1
        const val ADD_CONTACT_CODE = 1
        const val EDIT_CONTACT_CODE = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        iniciarComponentes()
        pedirPermisosWriteExternalStorage()
        pedirPermisosReadExternalStorage()
    }

    /**
     * Método para iniciar los componentes del Activity
     */
    private fun iniciarComponentes() {
        DBComunicator.initInstance(this) //Inicializar la base de datos
        exportImportController = ExportImportController()

        recyclerContactos = findViewById(R.id.RecyclerContactos)
        recyclerContactos!!.layoutManager = LinearLayoutManager(this)
        iniciarRecycler()

        fab = findViewById(R.id.fab)
        fab!!.setOnClickListener { _ ->
            startActivityForResult(Intent(applicationContext, AddContactActivity::class.java), ADD_CONTACT_CODE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == Activity.RESULT_OK) {
            //Si volvemos de crear un contacto, lo añadimos
            if (requestCode == ADD_CONTACT_CODE)
                addContacto(data!!.getIntExtra(DBContract.DBEntry.ID, 9999))

            //Si volvemos de editar un contacto, actualizamos el recycler
            else if (requestCode == EDIT_CONTACT_CODE) {
                contactos!![posicionContactoEditado!!] = DBComunicator.getInstance().getContacto(idContactoEditado!!)!!
                adapter!!.notifyItemChanged(posicionContactoEditado!!)
                adapter!!.notifyDataSetChanged()
            }
        }
    }

    /**
     * Método para actualizar el RecyclerView del Activity
     */
    private fun iniciarRecycler() {
        contactos = obtenerContactos()
        sortArrayContactos()
        //Obtener el adapter del contacto para añadírselo al RecyclerView de los grupos
        adapter = AdaptadorContacto(contactos!!, ::inflateMenuContactos, ::mostrarContacto)
        recyclerContactos!!.adapter = adapter
    }

    private fun mostrarContacto(cardView: CardView, id: Int, pos: Int) {
        cardView.setOnClickListener{
            val popupMenu = PopupMenu(cardView.context, cardView)
            popupMenu.menuInflater.inflate(R.menu.menu_tlf, popupMenu.menu)
            //Forzar a mostrar los iconos en el menú PopUp
            try {
                val fieldMPopup = PopupMenu::class.java.getDeclaredField("mPopup")
                fieldMPopup.isAccessible = true
                val mPopup = fieldMPopup.get(popupMenu)
                mPopup.javaClass
                        .getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                        .invoke(mPopup, true)
            } catch (e: Exception){

            } finally {
                popupMenu.show()
            }
            popupMenu.setOnMenuItemClickListener{ item: MenuItem? ->
                when (item!!.itemId) {
                    R.id.llamar_contacto -> {
                        val intent = Intent(Intent.ACTION_CALL)
                        intent.data = Uri.parse("tel:${DBComunicator.getInstance().getContacto(id)!!.movil}")

                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            if (ActivityCompat.shouldShowRequestPermissionRationale(this as Activity,
                                            Manifest.permission.CALL_PHONE))
                            else
                                ActivityCompat.requestPermissions(this,
                                        arrayOf(Manifest.permission.CALL_PHONE), PERMISSIONS_REQUEST_CALL_PHONE)
                        }
                        else
                            startActivity(intent)
                    }

                    R.id.mensaje_contacto -> {
                        val uri = Uri.parse("smsto:${DBComunicator.getInstance().getContacto(id)!!.movil}")
                        val intent = Intent(Intent.ACTION_SENDTO, uri)
                        intent.putExtra("sms_body", "")
                        startActivity(intent)
                    }

                    R.id.info_contacto -> {
                        val intent = Intent(this, AddContactActivity::class.java)
                        posicionContactoEditado = pos
                        idContactoEditado = id

                        intent.putExtra("mostrar_contacto", id)
                        startActivityForResult(intent, EDIT_CONTACT_CODE)
                    }
                }
                true
            }
        }
    }

    private fun inflateMenuContactos(menu: ImageView, id: Int, pos: Int) {
        menu.setOnClickListener{
            val popupMenu = PopupMenu(menu.context, menu)
            popupMenu.menuInflater.inflate(R.menu.menu_contact, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener{ item: MenuItem? ->
                when (item!!.itemId) {
                    R.id.editar_contacto -> {
                        posicionContactoEditado = pos //Guardamos la posición para modificar el contacto en el ArrayList una vez el usuario lo modifique
                        idContactoEditado = id //Guardamos el ID para sacar de la Base de Datos el contacto modificado cuando el usuario termine

                        val intent = Intent(this, AddContactActivity::class.java)
                        intent.putExtra("modificar_contacto", id)
                        startActivityForResult(intent, EDIT_CONTACT_CODE)
                    }

                    R.id.eliminar_contacto -> {
                        confirmarEliminacion(id, pos)
                    }
                }
                true
            }
            popupMenu.show()
        }
    }

    private fun confirmarEliminacion(id: Int, pos: Int) {
        val dialogClickListener = DialogInterface.OnClickListener { _, which ->
            when (which) {
                DialogInterface.BUTTON_POSITIVE -> {
                    eliminarContacto(id, pos)
                    Toast.makeText(this, "Contacto eliminado", Toast.LENGTH_LONG).show()
                }

                DialogInterface.BUTTON_NEGATIVE -> {

                }
            }
        }

        val builder = AlertDialog.Builder(this)
        builder.setMessage("¿Desea eliminar el contacto?").setPositiveButton("Sí", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show()
    }

    /**
     * Método para añadir un contacto al RecyclerView
     * @param id ID del contacto a insertar
     */
    private fun addContacto(id: Int) {
        contactos!!.add(DBComunicator.getInstance().getContacto(id)!!)
        adapter!!.notifyItemInserted(adapter!!.itemCount - 1)
        //TODO sortArrayContactos()
        adapter!!.notifyDataSetChanged()
    }

    /**
     * Método para eliminar un contacto del RecyclerView
     * @param id ID del contacto a eliminar
     * @param pos Posición que ocupa el contacto a eliminar
     */
    private fun eliminarContacto(id: Int, pos: Int) {
        //Borrar el contacto de la base de datos
        DBComunicator.getInstance().deleteContact(id)
        //Eliminar el contacto del array
        contactos!!.removeAt(pos)
        //Notificar al adaptador que un item ha sido borrado
        adapter!!.notifyItemRemoved(pos)
        adapter!!.notifyDataSetChanged()
    }

    private fun sortArrayContactos() {
        contactos = ArrayList(contactos!!.sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER) { it.nombre }))
    }

    /**
     * Método que obtiene de la Base de Datos los contactos
     * @return ArrayList con todos los contactos alojados en la Base de Datos local
     */
    private fun obtenerContactos(): ArrayList<Contacto>? {
        val arrayContactos = ArrayList<Contacto>()
        //Cursor que contiene los resultados de la llamada a la tabla "CONTACTOS"
        val cursor = DBComunicator.getInstance().cursorConsulta(DBContract.DBEntry.TABLA_CONTACTOS, null, null)

        //Bucle que va sacando del cursor todas los contactos
        while (cursor.moveToNext()) {
            //Comprobamos si el valor ID es null, si no lo es guardamos el número que corresponde
            //Se realiza esta comprobación para evitar obtener un 0 si el valor es null
            val id = if(cursor.isNull(cursor.getColumnIndex(DBContract.DBEntry.ID)))
                null
            else
                cursor.getInt(cursor.getColumnIndex(DBContract.DBEntry.ID))

            //Comprobamos si el valor MOVIL es null, si no lo es guardamos el número que corresponde
            val movil = if (cursor.isNull(cursor.getColumnIndex(DBContract.DBEntry.MOVIL)))
                null
            else
                cursor.getInt(cursor.getColumnIndex(DBContract.DBEntry.MOVIL))

            //Comprobamos si el valor TELEFONO es null
            val telefono = if (cursor.isNull(cursor.getColumnIndex(DBContract.DBEntry.TELEFONO)))
                null
            else
                cursor.getInt(cursor.getColumnIndex(DBContract.DBEntry.TELEFONO))

            //Obtenemos el objeto Contacto con los valores sacados de la base de datos
            val contacto = Contacto(id, cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.NOMBRE)), cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.DIRECCION)),
                  movil, telefono, cursor.getString(cursor.getColumnIndex(DBContract.DBEntry.CORREO)))

            //Guardamos el objeto Contacto en el array de contactos
            arrayContactos.add(contacto)
        }
        cursor.close()

        return arrayContactos
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        adapter!!.filtrar(newText!!.toLowerCase())
        return true
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun filter(contactos: ArrayList<Contacto>, query: String): ArrayList<Contacto> {
        val lowerQuery = query.toLowerCase()
        val filteredModelList = ArrayList<Contacto>()

        for(contacto in contactos)
            if(contacto.nombre.contains(lowerQuery))
                filteredModelList.add(contacto)

        return filteredModelList
    }

    /**
     * Método para tratar el item seleccionado de la interfaz
     * @param item Item que ha sido pulsado
     * @return Devuelve Boolean que contendrá el resultado de la acción
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            //Si pulsan el botón de añadir del menú de la toolbar del Activity
            R.id.action_search -> {

            }

            R.id.action_export ->
                exportImportController!!.exportContacts(contactos!!)

            R.id.action_import ->
                exportImportController!!.importContacts(this.baseContext, contactos!!, ::addContacto)
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * Método para añadir menú de añadir a la toolbar del Activity
     * @param menu Menu del Activity
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        this.menu = menu
        menuInflater.inflate(R.menu.menu_search, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchMenuItem = menu.findItem(R.id.action_search)
        val searchView = searchMenuItem.actionView as SearchView

        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.isSubmitButtonEnabled = true
        searchView.setOnQueryTextListener(this)

        return super.onCreateOptionsMenu(menu)
    }

    private fun pedirPermisosWriteExternalStorage() {
        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        PERMISSIONS_REQUEST_EXTERNAL_STORAGE)

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    private fun pedirPermisosReadExternalStorage() {
        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        PERMISSIONS_REQUEST_EXTERNAL_STORAGE)

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }
}
