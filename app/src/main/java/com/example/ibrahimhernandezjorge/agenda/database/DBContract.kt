package com.example.ibrahimhernandezjorge.agenda.database

object DBContract {
    class DBEntry {
        companion object {
            const val TABLA_CONTACTOS = "CONTACTOS"
            const val ID = "ID"
            const val NOMBRE = "NOMBRE"
            const val DIRECCION = "DIR"
            const val MOVIL = "MOV"
            const val TELEFONO = "TLF"
            const val CORREO = "MAIL"
        }
    }
}